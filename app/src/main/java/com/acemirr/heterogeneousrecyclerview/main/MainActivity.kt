package com.acemirr.heterogeneousrecyclerview.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.acemirr.heterogeneousrecyclerview.R
import com.acemirr.heterogeneousrecyclerview.RVListAdapter
import com.acemirr.heterogeneousrecyclerview.base.ViewModelFactory
import com.acemirr.heterogeneousrecyclerview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var viewModel:MainActivityViewModel
    lateinit var binding:ActivityMainBinding
    val adapter = RVListAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        viewModel= ViewModelProvider(this).get(MainActivityViewModel::class.java)
        binding.vm = viewModel

    }

    override fun onStart() {
        viewModel.setupData()
        viewModel.listHeterogeneous.observe(this, Observer {
            adapter.submitList(it)
            binding.rvMain.adapter = adapter
        })
        super.onStart()
    }
}
