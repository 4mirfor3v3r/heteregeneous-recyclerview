package com.acemirr.heterogeneousrecyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.acemirr.heterogeneousrecyclerview.databinding.ItemImageBinding
import com.acemirr.heterogeneousrecyclerview.databinding.ItemTextBinding
import com.acemirr.heterogeneousrecyclerview.model.ImageModel
import com.acemirr.heterogeneousrecyclerview.model.TextModel

class RVListAdapter:ListAdapter<Any, RecyclerView.ViewHolder>(DiffUtil_Callback) {
    companion object {
        const val ITEM_IMAGE = 0
        const val ITEM_TEXT = 1

        val DiffUtil_Callback = object : DiffUtil.ItemCallback<Any>() {
            override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
                return if (oldItem is ImageModel && newItem is ImageModel) {
                    oldItem.title == newItem.title
                } else if (oldItem is TextModel && newItem is TextModel) {
                    oldItem.title == newItem.title
                } else {
                    true
                }
            }

            override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position) is ImageModel) {
            ITEM_IMAGE
        } else {
            ITEM_TEXT
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM_IMAGE) {
            val binding: ItemImageBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_image, parent, false)
            ItemImageViewHolder(binding)
        } else {
            val binding: ItemTextBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_text, parent, false)
            ItemTextViewHolder(binding)
        }
    }

    class ItemTextViewHolder(private val binding: ItemTextBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindData(textModel: TextModel) {
            binding.data = textModel
            binding.executePendingBindings()
        }
    }

    class ItemImageViewHolder(private val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindData(imageModel: ImageModel) {
            binding.data = imageModel
            binding.executePendingBindings()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemImageViewHolder) {
            val imageModel = getItem(holder.adapterPosition) as ImageModel
            holder.bindData(imageModel)
        } else if (holder is ItemTextViewHolder) {
            val textModel = getItem(holder.adapterPosition) as TextModel
            holder.bindData(textModel)
        }
    }
}