package com.acemirr.heterogeneousrecyclerview.model

data class TextModel(var title:String, var type:String)