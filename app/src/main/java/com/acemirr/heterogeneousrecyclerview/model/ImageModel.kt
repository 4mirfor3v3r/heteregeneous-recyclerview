package com.acemirr.heterogeneousrecyclerview.model

data class ImageModel(
    var title:String,
    var imageUrl:String
)