@file:Suppress("UNCHECKED_CAST")

package com.acemirr.heterogeneousrecyclerview.base

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.acemirr.heterogeneousrecyclerview.main.MainActivity
import com.acemirr.heterogeneousrecyclerview.main.MainActivityViewModel

class ViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(MainActivity::class.java) -> MainActivityViewModel() as T
            else -> throw IllegalArgumentException("Unknown Class Name")
        }
    }
}