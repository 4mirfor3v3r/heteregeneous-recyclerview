package com.acemirr.heterogeneousrecyclerview

import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator

object BindingExtensions {
    private val picasso: Picasso
        get() = Picasso.get()

    private fun ImageView.load(path: String?, request: (RequestCreator) -> RequestCreator) {
        if (!path.isNullOrEmpty()) {
            request(picasso.load(path))
                .priority(Picasso.Priority.HIGH)
                .placeholder(R.drawable.ic_launcher_background)
                .into(this)
        }
    }

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, url: String?) {
        if (url != null) {
            view.load(url) { requestCreator ->
                requestCreator.fit().centerCrop()
            }
        }
    }
}